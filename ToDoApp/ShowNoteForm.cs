﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDoApp.Data;

namespace ToDoApp
{
    public partial class ShowNoteForm : MaterialForm
    {
        private Note _note { get; set; } 

        public ShowNoteForm(Note note)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Yellow800, Primary.Yellow900, Primary.Yellow500, Accent.Red200, TextShade.WHITE);

            _note = note;
            InitializeComponent();

            this.Text = note.Title;
            message.Text = note.Message;
            date.Text = note.Added.ToString();
        }
    }
}
