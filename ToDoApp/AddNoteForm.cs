﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDoApp.Data;

namespace ToDoApp
{
    public partial class AddNoteForm : MaterialForm
    {
        public Note note
        {
            get
            {
                return _note;
            }
        }
        private Note _note;

        public AddNoteForm()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Yellow800, Primary.Yellow900, Primary.Yellow500, Accent.Red200, TextShade.WHITE);
            this.ActiveControl = message;
        }

        private void add_Click(object sender, EventArgs e)
        {
            _note = new Note() { Title = title.Text, Message = message.Text, Added = DateTime.Now };
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
