﻿namespace ToDoApp
{
    partial class AddNoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.message = new System.Windows.Forms.TextBox();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.add = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Depth = 0;
            this.title.Hint = "Title";
            this.title.Location = new System.Drawing.Point(13, 79);
            this.title.MaxLength = 32767;
            this.title.MouseState = MaterialSkin.MouseState.HOVER;
            this.title.Name = "title";
            this.title.PasswordChar = '\0';
            this.title.SelectedText = "";
            this.title.SelectionLength = 0;
            this.title.SelectionStart = 0;
            this.title.Size = new System.Drawing.Size(275, 23);
            this.title.TabIndex = 0;
            this.title.TabStop = false;
            this.title.UseSystemPasswordChar = false;
            // 
            // message
            // 
            this.message.BackColor = System.Drawing.Color.White;
            this.message.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.message.Location = new System.Drawing.Point(13, 136);
            this.message.MaxLength = 255;
            this.message.Multiline = true;
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(275, 104);
            this.message.TabIndex = 2;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(12, 114);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(101, 19);
            this.materialLabel1.TabIndex = 3;
            this.materialLabel1.Text = "Note content:";
            // 
            // add
            // 
            this.add.AutoSize = true;
            this.add.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.add.Depth = 0;
            this.add.Icon = null;
            this.add.Location = new System.Drawing.Point(201, 252);
            this.add.MouseState = MaterialSkin.MouseState.HOVER;
            this.add.Name = "add";
            this.add.Primary = true;
            this.add.Size = new System.Drawing.Size(87, 36);
            this.add.TabIndex = 4;
            this.add.Text = "Add note";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // AddNoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 300);
            this.Controls.Add(this.add);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.message);
            this.Controls.Add(this.title);
            this.Name = "AddNoteForm";
            this.Text = "Add note";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField title;
        private System.Windows.Forms.TextBox message;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialRaisedButton add;
    }
}