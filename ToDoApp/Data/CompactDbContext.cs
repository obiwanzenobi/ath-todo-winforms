﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoApp.Data
{
    public class CompactDbContext : DbContext
    {
        public const string CONNECTION_STRING = "Data Source='ToDo.sdf'; LCID=1033;   Password=todoApp1037; Encrypt = TRUE;";

        public CompactDbContext() : base(new SqlCeConnection(CONNECTION_STRING), contextOwnsConnection: true)
        {

        }
        public DbSet<Note> Notes { get; set; }
    }
}
