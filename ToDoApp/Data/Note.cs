﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoApp.Data
{
    public class Note
    {
        public int NoteId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime Added { get; set; }
    }
}
