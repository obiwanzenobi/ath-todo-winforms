﻿namespace ToDoApp
{
    partial class ShowNoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.message = new MaterialSkin.Controls.MaterialLabel();
            this.date = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // message
            // 
            this.message.Depth = 0;
            this.message.Font = new System.Drawing.Font("Roboto", 11F);
            this.message.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.message.Location = new System.Drawing.Point(12, 99);
            this.message.MouseState = MaterialSkin.MouseState.HOVER;
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(275, 192);
            this.message.TabIndex = 0;
            // 
            // date
            // 
            this.date.AutoSize = true;
            this.date.Depth = 0;
            this.date.Font = new System.Drawing.Font("Roboto", 11F);
            this.date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.date.Location = new System.Drawing.Point(12, 71);
            this.date.MouseState = MaterialSkin.MouseState.HOVER;
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(0, 19);
            this.date.TabIndex = 1;
            // 
            // ShowNoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 300);
            this.Controls.Add(this.date);
            this.Controls.Add(this.message);
            this.Name = "ShowNoteForm";
            this.Text = "ShowNoteForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel message;
        private MaterialSkin.Controls.MaterialLabel date;
    }
}