﻿using System;
using System.Linq;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.IO;
using ToDoApp.Data;
using System.Windows.Forms;
using MaterialSkin.Controls;
using MaterialSkin;
using System.Collections.Generic;

namespace ToDoApp
{
    public partial class MainForm : MaterialForm
    {
        private CompactDbContext _db;
        private List<Note> _itemList;

        public MainForm()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Yellow800, Primary.Yellow900, Primary.Yellow500, Accent.Red200, TextShade.WHITE);

            CreateDbIfNotExists();
            _db = new CompactDbContext();

            _itemList = _db.Notes.ToList();
            _itemList.ForEach(element =>
            {
                AddElementToList(element);
            });


        }

        private void AddElementToList(Note element)
        {
            ListViewItem item = new ListViewItem(element.Title);
            item.SubItems.Add(element.Added.ToString());
            materialListView1.Items.Add(item);
        }

        private static void CreateDbIfNotExists()
        {

            if (!File.Exists("ToDo.sdf"))
            {
                SqlCeEngine engine = new SqlCeEngine(CompactDbContext.CONNECTION_STRING);
                engine.CreateDatabase();
            }
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            using (var form = new AddNoteForm())
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Note newNote = form.note;

                    _db.Notes.Add(newNote);
                    _db.SaveChanges();

                    _itemList.Add(newNote);
                    AddElementToList(newNote);

                    materialListView1.Items[materialListView1.Items.Count - 1].EnsureVisible();
                }
            }
        }

        private void materialListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItemIndex = materialListView1.SelectedItems[0].Index;
            ShowNoteForm showNoteForm = new ShowNoteForm(_itemList[selectedItemIndex]);
            showNoteForm.Show();
        }
    }
}
